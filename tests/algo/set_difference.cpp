// Copyright © 2012, 2013, 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/algo/set_difference.hpp>
#include <hopp/print/std.hpp>


int main()
{
	std::cout << "Test #include <hopp/algo/set_difference.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	++nb_test;
	{
		std::vector<int> a = { 0, 1, 2, 3, 4, 5 };
		std::list<int>   b = {    1,       4, 5, 6, 7, 8, 9 };
		
		std::sort(a.begin(), a.end());
		b.sort();
		
		std::vector<int> const r = hopp::algo::set_difference(a, b);
		
		std::cout << r << std::endl; // 0, 2, 3
		
		nb_test -= hopp::test(r == std::vector<int>({ 0, 2, 3 }), "hopp::algo::set_difference fails\n");
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp::algo::set_difference: " + hopp::to_string(nb_test) + " test fail!\n");
	
	return 0;
}
