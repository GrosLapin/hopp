// Copyright © 2012, 2013, 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>
#include <string>
#include <vector>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/algo/to_upper.hpp>


int main()
{
	std::cout << "Test #include <hopp/algo/to_upper.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	++nb_test;
	{
		std::string s("A example with std::string.");
		
		std::cout << s << std::endl;
		hopp::algo::to_upper(s);
		std::cout << s << std::endl;
		std::cout << std::endl;
		
		nb_test -= hopp::test(s == "A EXAMPLE WITH STD::STRING.", "hopp::algo::to_upper fails\n");
	}

	++nb_test;
	{
		std::vector<char> s({ 'A', ' ', 'e', 'x', 'a', 'm', 'p', 'l', 'e', ' ', 'w', 'i', 't', 'h', ' ', 's', 't', 'd', ':', ':', 'v', 'e', 'c', 't', 'o', 'r', '<', 'c', 'h', 'a', 'r', '>', '.' });
		
		for (char const c : s) { std::cout << c; } std::cout << std::endl;
		hopp::algo::to_upper(s);
		for (char const c : s) { std::cout << c; } std::cout << std::endl;
		std::cout << std::endl;
		
		nb_test -= hopp::test(s == std::vector<char>({ 'A', ' ', 'E', 'X', 'A', 'M', 'P', 'L', 'E', ' ', 'W', 'I', 'T', 'H', ' ', 'S', 'T', 'D', ':', ':', 'V', 'E', 'C', 'T', 'O', 'R', '<', 'C', 'H', 'A', 'R', '>', '.' }), "hopp::algo::to_upper fails\n");
	}

	++nb_test;
	{
		std::string s("A example with std::string.");
		
		std::cout << s << std::endl;
		auto const copy = hopp::algo::to_upper_copy(s);
		std::cout << s << std::endl;
		std::cout << copy << std::endl;
		std::cout << std::endl;
		
		nb_test -= hopp::test(s == "A example with std::string." && copy == "A EXAMPLE WITH STD::STRING.", "hopp::algo::to_upper fails\n");
	}

	++nb_test;
	{
		std::vector<char> s({ 'A', ' ', 'e', 'x', 'a', 'm', 'p', 'l', 'e', ' ', 'w', 'i', 't', 'h', ' ', 's', 't', 'd', ':', ':', 'v', 'e', 'c', 't', 'o', 'r', '<', 'c', 'h', 'a', 'r', '>', '.' });
		
		for (char const c : s) { std::cout << c; } std::cout << std::endl;
		auto const copy = hopp::algo::to_upper_copy(s);
		for (char const c : s) { std::cout << c; } std::cout << std::endl;
		for (char const c : copy) { std::cout << c; } std::cout << std::endl;
		std::cout << std::endl;
		
		nb_test -= hopp::test(s == std::vector<char>({ 'A', ' ', 'e', 'x', 'a', 'm', 'p', 'l', 'e', ' ', 'w', 'i', 't', 'h', ' ', 's', 't', 'd', ':', ':', 'v', 'e', 'c', 't', 'o', 'r', '<', 'c', 'h', 'a', 'r', '>', '.' }) && copy == std::vector<char>({ 'A', ' ', 'E', 'X', 'A', 'M', 'P', 'L', 'E', ' ', 'W', 'I', 'T', 'H', ' ', 'S', 'T', 'D', ':', ':', 'V', 'E', 'C', 'T', 'O', 'R', '<', 'C', 'H', 'A', 'R', '>', '.' }), "hopp::algo::to_upper fails\n");
	}
	
	hopp::test(nb_test == 0, "hopp::algo::to_upper: " + hopp::to_string(nb_test) + " test fail!\n");
	
	return 0;
}
