#include <iostream>
#include <string>
#include <cassert>
#include "../../include/hopp/type/parameters_information.hpp"




 int foo (double i, int a){ 
    return 42*a+(int)i;
} 





class foncteur
{
    public :
        int operator() (int a,int b ,std::string c );
};

int main()
{
    using namespace std;
    using namespace hopp;


    auto lambda = [](int i) { return long(i*10); };


   foncteur f;


   assert ( nbParam(f) == nbParam<foncteur>() );
   assert ( nbParam(foo) == nbParam<decltype(foo)>() );
   assert ( nbParam(lambda) == nbParam<decltype(lambda)>() );

    static_assert( std::is_same<decltype(typeParam<0>(foo)),decltype(typeParam<0,decltype(foo)>())>::value, "erreur ");
    static_assert( std::is_same<decltype(typeParam<0>(f)),decltype(typeParam<0,decltype(f)>())>::value, "erreur ");
    static_assert( std::is_same<decltype(typeParam<0>(lambda)),decltype(typeParam<0,decltype(lambda)>())>::value, "erreur ");


    static_assert( std::is_same<decltype(typeRetour(foo)),decltype(typeRetour<decltype(foo)>())>::value, "erreur ");
    static_assert( std::is_same<decltype(typeRetour(f)),decltype(typeRetour<decltype(f)>())>::value, "erreur ");
    static_assert( std::is_same<decltype(typeRetour(lambda)),decltype(typeRetour<decltype(lambda)>())>::value, "erreur ");

    assert( nbParam(f) == 3);
    assert( nbParam(foo) == 2);
    assert( nbParam(lambda) == 1);


    static_assert( std::is_same<decltype(typeParam<0>(foo)),double>::value, "erreur ");
    static_assert( std::is_same<decltype(typeParam<1>(foo)),int>::value, "erreur ");

    static_assert( std::is_same<decltype(typeParam<0>(f)),int>::value, "erreur ");
    static_assert( std::is_same<decltype(typeParam<1>(f)),int>::value, "erreur ");
    static_assert( std::is_same<decltype(typeParam<2>(f)),std::string>::value, "erreur ");

    static_assert( std::is_same<decltype(typeParam<0>(lambda)),int>::value, "erreur ");

    return 0;
}
