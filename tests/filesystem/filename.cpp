// Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr
// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <hopp/test.hpp>
#include <hopp/filesystem.hpp>
#include <hopp/conversion/to_string.hpp>


int main()
{
	std::cout << "Test #include <hopp/filesystem.hpp>" << std::endl;
	
	int nb_test = 0;
	
	// Filename
	
	++nb_test;
	{
		std::string const s = "/path/to/file.ext";
		std::string const r = hopp::filesystem::filename(s);
		std::cout << "Filename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "file.ext", "hopp::filesystem::filename fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "file.ext";
		std::string const r = hopp::filesystem::filename(s);
		std::cout << "Filename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "file.ext", "hopp::filesystem::filename fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "/path/to/";
		std::string const r = hopp::filesystem::filename(s);
		std::cout << "Filename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "", "hopp::filesystem::filename fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "";
		std::string const r = hopp::filesystem::filename(s);
		std::cout << "Filename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "", "hopp::filesystem::filename fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "f";
		std::string const r = hopp::filesystem::filename(s);
		std::cout << "Filename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "f", "hopp::filesystem::filename fails\n");
	}
	std::cout << std::endl;
	
	// Dirname
	
	++nb_test;
	{
		std::string const s = "/path/to/file.ext";
		std::string const r = hopp::filesystem::dirname(s);
		std::cout << "Dirname of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "/path/to", "hopp::filesystem::dirname fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "file.ext";
		std::string const r = hopp::filesystem::dirname(s);
		std::cout << "Dirname of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "", "hopp::filesystem::dirname fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "/path/to/";
		std::string const r = hopp::filesystem::dirname(s);
		std::cout << "Dirname of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "/path/to", "hopp::filesystem::dirname fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "";
		std::string const r = hopp::filesystem::dirname(s);
		std::cout << "Dirname of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "", "hopp::filesystem::dirname fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "/";
		std::string const r = hopp::filesystem::dirname(s);
		std::cout << "Dirname of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "", "hopp::filesystem::dirname fails\n");
	}
	std::cout << std::endl;
	
	// Basename
	
	++nb_test;
	{
		std::string const s = "/path/to/file.ext";
		std::string const r = hopp::filesystem::basename(s);
		std::cout << "Basename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "file", "hopp::filesystem::basename fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "/p.a.t.h/t.o/f.i.l.e.ext";
		std::string const r = hopp::filesystem::basename(s);
		std::cout << "Basename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "f.i.l.e", "hopp::filesystem::basename fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "file.ext";
		std::string const r = hopp::filesystem::basename(s);
		std::cout << "Basename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "file", "hopp::filesystem::basename fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "/path/to/";
		std::string const r = hopp::filesystem::basename(s);
		std::cout << "Basename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "", "hopp::filesystem::basename fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "";
		std::string const r = hopp::filesystem::basename(s);
		std::cout << "Basename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "", "hopp::filesystem::basename fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "f";
		std::string const r = hopp::filesystem::basename(s);
		std::cout << "Basename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "f", "hopp::filesystem::basename fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "f.";
		std::string const r = hopp::filesystem::basename(s);
		std::cout << "Basename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "f", "hopp::filesystem::basename fails\n");
	}
	std::cout << std::endl;
	
	// Dirname and Basename
	
	++nb_test;
	{
		std::string const s = "/path/to/file.ext";
		std::string const r = hopp::filesystem::dir_and_basename(s);
		std::cout << "Dirname and Basename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "/path/to/file", "hopp::filesystem::dir_and_basename fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "file.ext";
		std::string const r = hopp::filesystem::dir_and_basename(s);
		std::cout << "Dirname and Basename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "file", "hopp::filesystem::dir_and_basename fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "/path/to/";
		std::string const r = hopp::filesystem::dir_and_basename(s);
		std::cout << "Dirname and Basename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "/path/to/", "hopp::filesystem::dir_and_basename fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "";
		std::string const r = hopp::filesystem::dir_and_basename(s);
		std::cout << "Dirname and Basename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "", "hopp::filesystem::dir_and_basename fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "f";
		std::string const r = hopp::filesystem::dir_and_basename(s);
		std::cout << "Dirname and Basename of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "f", "hopp::filesystem::dir_and_basename fails\n");
	}
	std::cout << std::endl;
	
	// Extension
	
	++nb_test;
	{
		std::string const s = "/path/to/file.ext";
		std::string const r = hopp::filesystem::extension(s);
		std::cout << "Extension of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "ext", "hopp::filesystem::extension fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "/p.a.t.h/t.o/f.i.l.e.ext";
		std::string const r = hopp::filesystem::extension(s);
		std::cout << "Extension of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "ext", "hopp::filesystem::extension fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "file.ext";
		std::string const r = hopp::filesystem::extension(s);
		std::cout << "Extension of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "ext", "hopp::filesystem::extension fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "fi.le.ext";
		std::string const r = hopp::filesystem::extension(s);
		std::cout << "Extension of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "ext", "hopp::filesystem::extension fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "/path/to/";
		std::string const r = hopp::filesystem::extension(s);
		std::cout << "Extension of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "", "hopp::filesystem::extension fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "";
		std::string const r = hopp::filesystem::extension(s);
		std::cout << "Extension of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "", "hopp::filesystem::extension fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "f";
		std::string const r = hopp::filesystem::extension(s);
		std::cout << "Extension of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "", "hopp::filesystem::extension fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "f.";
		std::string const r = hopp::filesystem::extension(s);
		std::cout << "Extension of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "", "hopp::filesystem::extension fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "f.e";
		std::string const r = hopp::filesystem::extension(s);
		std::cout << "Extension of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "e", "hopp::filesystem::extension fails\n");
	}
	
	++nb_test;
	{
		std::string const s = ".";
		std::string const r = hopp::filesystem::extension(s);
		std::cout << "Extension of \"" << s << "\" is \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "", "hopp::filesystem::extension fails\n");
	}
	std::cout << std::endl;
	
	// Add prefix and suffix
	
	++nb_test;
	{
		std::string const s = "/path/to/file.ext";
		std::string const r = hopp::filesystem::add_prefix(s, "prefix_");
		std::cout << "Add prefix: \"" << s << "\" become \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "/path/to/prefix_file.ext", "hopp::filesystem::add_prefix fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "/path/to/file";
		std::string const r = hopp::filesystem::add_prefix(s, "prefix_");
		std::cout << "Add prefix: \"" << s << "\" become \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "/path/to/prefix_file", "hopp::filesystem::add_prefix fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "file.ext";
		std::string const r = hopp::filesystem::add_prefix(s, "prefix_");
		std::cout << "Add prefix: \"" << s << "\" become \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "prefix_file.ext", "hopp::filesystem::add_prefix fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "file";
		std::string const r = hopp::filesystem::add_prefix(s, "prefix_");
		std::cout << "Add prefix: \"" << s << "\" become \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "prefix_file", "hopp::filesystem::add_prefix fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "/path/to/file.ext";
		std::string const r = hopp::filesystem::add_suffix(s, "_suffix");
		std::cout << "Add suffix: \"" << s << "\" become \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "/path/to/file_suffix.ext", "hopp::filesystem::add_suffix fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "/path/to/file";
		std::string const r = hopp::filesystem::add_suffix(s, "_suffix");
		std::cout << "Add suffix: \"" << s << "\" become \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "/path/to/file_suffix", "hopp::filesystem::add_suffix fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "file.ext";
		std::string const r = hopp::filesystem::add_suffix(s, "_suffix");
		std::cout << "Add suffix: \"" << s << "\" become \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "file_suffix.ext", "hopp::filesystem::add_suffix fails\n");
	}
	
	++nb_test;
	{
		std::string const s = "file";
		std::string const r = hopp::filesystem::add_suffix(s, "_suffix");
		std::cout << "Add suffix: \"" << s << "\" become \"" << r << "\"" << std::endl;
		nb_test -= hopp::test(r == "file_suffix", "hopp::filesystem::add_suffix fails\n");
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp/filesystem/filename.hpp: " + hopp::to_string(nb_test) + " test fail!\n");
	
	return nb_test;
}
