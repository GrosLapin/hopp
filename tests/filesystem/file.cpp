// Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr
// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <hopp/test.hpp>
#include <hopp/filesystem.hpp>
#include <hopp/conversion/to_string.hpp>


int main()
{
	std::cout << "Test #include <hopp/filesystem.hpp>" << std::endl;
	
	int nb_test = 0;
	
	// Tmp filename & remove

	for (unsigned int i = 0; i < 10; ++i)
	{
		nb_test += 2;
		std::string const r = hopp::filesystem::tmp_filename();
		std::cout << "Tmp filename = \"" << r << "\"" << std::endl;
		{
			std::fstream f(r);
			nb_test -= hopp::test(f.is_open() == true, "hopp::filesystem::tmp_filename fails\n");
		}
		hopp::filesystem::remove(r);
		{
			std::fstream f(r);
			nb_test -= hopp::test(f.is_open() == false, "hopp::filesystem::remove fails\n");
		}
	}
	std::cout << std::endl;

	// read_file

	++nb_test;
	{
		std::cout << "hopp::filesystem::read_file" << std::endl;
		// Write
		std::string const txt = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. \nSed non risus. \nSuspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor.\n";
		std::cout << "txt:" << std::endl << txt;
		std::string const filename = hopp::filesystem::tmp_filename();
		{
			std::ofstream f(filename);
			f << txt;
		}
		// Read
		std::string const read_txt = hopp::filesystem::read_file(filename);
		std::cout << "txt:" << std::endl << read_txt;
		nb_test -= hopp::test(read_txt == txt, "hopp::filesystem::read_file fails\n");
		// Remove
		hopp::filesystem::remove(filename);
	}
	std::cout << std::endl;

	// copy_file

	++nb_test;
	{
		std::cout << "hopp::filesystem::copy_file" << std::endl;
		// txt
		std::string const txt = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. \nSed non risus. \nSuspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor.\n";
		std::cout << "txt:" << std::endl << txt;
		// Source
		std::string const src_filename = hopp::filesystem::tmp_filename();
		{
			std::ofstream f(src_filename);
			f << txt;
		}
		// Destination
		std::string const dest_filename = hopp::filesystem::filename_without_overwrite(src_filename);
		hopp::filesystem::copy_file(src_filename, dest_filename);
		std::string const copy_txt = hopp::filesystem::read_file(dest_filename);
		std::cout << "copy_txt:" << std::endl << copy_txt;
		nb_test -= hopp::test(copy_txt == txt, "hopp::filesystem::copy_file fails\n");
		// Remove
		hopp::filesystem::remove(src_filename);
		hopp::filesystem::remove(dest_filename);
	}
	std::cout << std::endl;

	// File exists & filename_without_overwrite

	++nb_test;
	{
		std::string const r = hopp::filesystem::tmp_filename();
		std::cout << "File \"" << r << "\" must exist" << std::endl;
		nb_test -= hopp::test(hopp::filesystem::file_exists(r) == true && hopp::filesystem::file_is_readable(r) == true && hopp::filesystem::file_is_writeable(r) == true, "hopp::filesystem::file_exists fails\n");
		hopp::filesystem::remove(r);
	}

	++nb_test;
	{
		std::string const r = hopp::filesystem::tmp_filename();
		hopp::filesystem::remove(r);
		std::cout << "File \"" << r << "\" must not exist" << std::endl;
		nb_test -= hopp::test(hopp::filesystem::file_exists(r) == false && hopp::filesystem::file_is_readable(r) == false && hopp::filesystem::file_is_writeable(r) == false, "hopp::filesystem::file_exists fails\n");
	}
	std::cout << std::endl;

	++nb_test;
	{
		std::string const r = hopp::filesystem::tmp_filename();
		std::string const n = hopp::filesystem::filename_without_overwrite(r);
		std::cout << "File \"" << r << "\" exists, generate \"" << n << "\"" << std::endl;
		nb_test -= hopp::test(hopp::filesystem::file_exists(n) == false, "hopp::filesystem::filename_without_overwrite fails\n");
		hopp::filesystem::remove(r);
	}

	++nb_test;
	{
		std::string const f0 = hopp::filesystem::tmp_filename();
		std::string const n0 = hopp::filesystem::filename_without_overwrite(f0);
		{ std::ofstream f(n0); }
		std::string const n1 = hopp::filesystem::filename_without_overwrite(f0);
		std::cout << "File \"" << f0 << "\" exists, generate \"" << n0 << "\" and generate \"" << n1 << "\"" << std::endl;
		nb_test -= hopp::test(hopp::filesystem::file_exists(n0) == true && hopp::filesystem::file_exists(n1) == false, "hopp::filesystem::filename_without_overwrite fails\n");
		hopp::filesystem::remove(f0);
		hopp::filesystem::remove(n0);
	}
	std::cout << std::endl;

	nb_test += 5;
	{
		std::string const path = hopp::filesystem::filename_without_overwrite(hopp::filesystem::tmp_filename());
		std::cout << "Create directory " << path << std::endl;
		bool const r = hopp::filesystem::create_directory(path);
		{ std::ofstream f(path + "/hopp_tmp"); }
		nb_test -= hopp::test(r && hopp::filesystem::file_exists(path + "/hopp_tmp"), "hopp::filesystem::create_directory fails\n");
		nb_test -= hopp::test(r && hopp::filesystem::is_a_directory(path), "hopp::filesystem::is_a_directory fails\n");
		nb_test -= hopp::test(r && hopp::filesystem::is_a_file(path) == false, "hopp::filesystem::is_a_file fails\n");
		nb_test -= hopp::test(r && hopp::filesystem::is_a_directory(path + "/hopp_tmp") == false, "hopp::filesystem::is_a_directory fails\n");
		nb_test -= hopp::test(r && hopp::filesystem::is_a_file(path + "/hopp_tmp"), "hopp::filesystem::is_a_file fails\n");
	}
	std::cout << std::endl;

	++nb_test;
	{
		if (hopp::filesystem::file_exists("/proc/cpuinfo"))
		{
			nb_test -= hopp::test(hopp::filesystem::file_is_writeable("/proc/cpuinfo") == false, "hopp::filesystem::file_is_writeable fails with \"/proc/cpuinfo\"\n\n");
		}
		else
		{
			std::cout << "/proc/cpuinfo does not exist, skip tst of hopp::filesystem::file_is_writeable" << std::endl;
			std::cout << std::endl;
			--nb_test;
		}
	}
	
	hopp::test(nb_test == 0, "hopp/filesystem/file.hpp: " + hopp::to_string(nb_test) + " test fail!\n");
	
	return 0;
}
