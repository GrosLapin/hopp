// Copyright © 2012, 2013, 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <hopp/test.hpp>
#include <hopp/conversion.hpp>


int main()
{
	std::cout << "Test #include <hopp/conversion/to_type.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	std::cout << "Doxygen exemples:" << std::endl;
	nb_test += 2;
	{
		int i = hopp::to_<int>("42"); // You can use hopp::to_int
		double d = hopp::to_<double>("42.1"); // You can use hopp::to_double
		
		std::cout << "i = " << i << std::endl;
		std::cout << "d = " << d << std::endl;
		
		nb_test -= hopp::test(i == 42, "hopp::to_<int>(\"42\") fails!\n");
		nb_test -= hopp::test(d == 42.1, "hopp::to_<double>(\"42.1\") fails!\n");
	}
	std::cout << std::endl;
	
	nb_test += 2;
	{
		int i = hopp::to_int("42");
		double d = hopp::to_double("42.1");
		
		std::cout << "i = " << i << std::endl;
		std::cout << "d = " << d << std::endl;
		
		nb_test -= hopp::test(i == 42, "hopp::to_<int>(\"42\") fails!\n");
		nb_test -= hopp::test(d == 42.1, "hopp::to_<double>(\"42.1\") fails!\n");
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp::to_type: " + hopp::to_string(nb_test) + " test fail!\n");
	
	return 0;
}
