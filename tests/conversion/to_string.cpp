// Copyright © 2012, 2013, 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>


int main()
{
	std::cout << "Test #include <hopp/conversion/to_string.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	std::cout << "Doxygen exemples:" << std::endl;
	{
		std::string s = hopp::to_string("A int = ", 5, ", and a char = ", 'a');
		std::cout << s << std::endl;
	}
	{
		std::string s = hopp::to_string("A double (full precision) = ", std::setprecision(std::numeric_limits<double>::digits10 + 1), 3.14);
		std::cout << s << std::endl;
	}
	{
		std::string s = hopp::to_string("A bool = ", true, ", with std::boolalpha = ", std::boolalpha, true);
		std::cout << s << std::endl;
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		int i = 56648;
		std::string s = hopp::to_string(i);
		std::cout << i << "\n" << s << std::endl;
		nb_test -= hopp::test(s == "56648", "hopp::to_string fails!\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		double i = 7578.7;
		std::string s = hopp::to_string(i);
		std::cout << i << "\n" << s << std::endl;
		nb_test -= hopp::test(s == "7578.7", "hopp::to_string fails!\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		double i = -54664.6546;
		std::string s = hopp::to_string(std::setprecision(std::numeric_limits<double>::digits10 + 1), i);
		std::cout << std::setprecision(std::numeric_limits<double>::digits10 + 1) << i << "\n" << s << std::endl;
		nb_test -= hopp::test(s == "-54664.6546", "hopp::to_string fails!\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		char i = 'i';
		std::string s = hopp::to_string(i);
		std::cout << i << "\n" << s << std::endl;
		nb_test -= hopp::test(s == "i", "hopp::to_string fails!\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::string i = "a std::string";
		std::string s = hopp::to_string(i);
		std::cout << i << "\n" << s << std::endl;
		nb_test -= hopp::test(s == i, "hopp::to_string fails!\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		char const * const i = "a char const * const";
		std::string s = hopp::to_string(i);
		std::cout << i << "\n" << s << std::endl;
		nb_test -= hopp::test(s == i, "hopp::to_string fails!\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		bool i = true;
		std::string s = hopp::to_string(i);
		std::cout << i << "\n" << s << std::endl;
		nb_test -= hopp::test(s == "1", "hopp::to_string fails!\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		bool i = true;
		std::string s = hopp::to_string(std::boolalpha, i);
		std::cout << i << "\n" << s << std::endl;
		nb_test -= hopp::test(s == "true", "hopp::to_string faisl!\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::string s = hopp::to_string("a char []", " | ", std::string("a std::string"), " | ", "some numbers: ", 21, ' ', 73.42, ' ', 3.14f, " | ", "bools without and with std::boolalpha: ", true, ' ', false, ' ', std::boolalpha, true, ' ', false);
		std::cout << s << "\n" << "a char [] | a std::string | some numbers: 21 73.42 3.14 | bools without and with std::boolalpha: 1 0 true false" << std::endl;
		nb_test -= hopp::test(s == "a char [] | a std::string | some numbers: 21 73.42 3.14 | bools without and with std::boolalpha: 1 0 true false", "hopp::to_string fails!\n");
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp::to_string: " + hopp::to_string(nb_test) + " test fail!\n");
	
	return 0;
}
