// Copyright © 2014, 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>
#include <string>
#include <vector>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/print/print.hpp>
#include <hopp/time/sleep.hpp>
#include <hopp/random.hpp>


void test_print(bool const mutex)
{
	auto const print_fct = []() -> void
	{
		hopp::sleep::nanoseconds(hopp::random::uniform(1u, 3u));
		hopp::print("Test", ' ', "hopp::print", ':', ' ', 42, ", ", 3.14, ", ", std::string("std::string"), '\n');
	};
	
	auto const print_mutex_fct = []() -> void
	{
		hopp::sleep::nanoseconds(hopp::random::uniform(1u, 3u));
		hopp::print_mutex("Test", ' ', "hopp::print", ':', ' ', 42, ", ", 3.14, ", ", std::string("std::string"), '\n');
	};
	
	std::vector<std::thread> threads;
	for (unsigned int i = 0; i < 33; ++i)
	{
		threads.emplace_back(mutex ? print_mutex_fct : print_fct);
	}
	
	for (auto & thread : threads)
	{
		thread.join();
	}
}


int main()
{
	std::cout << "Test #include <hopp/stream/print.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	std::cout << "hopp::print" << std::endl;
	test_print(false);
	std::cout << std::endl;
	
	std::cout << "hopp::print_mutex" << std::endl;
	test_print(true);
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp::to_string: " + hopp::to_string(nb_test) + " test fail!\n");
	
	return 0;
}
