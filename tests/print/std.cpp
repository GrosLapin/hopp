// Copyright © 2014, 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>
#include <string>
#include <vector>
#include <set>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/print/std.hpp>


int main()
{
	std::cout << "Test #include <hopp/stream/ostreamable.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	{
		std::vector<std::vector<int>> c(3, std::vector<int>(2));
		for (size_t i = 0; i < c.size(); ++i)
		{
			for (size_t j = 0; j < c[i].size(); ++j)
			{ c[i][j] = int(i + j) * 10; }
		}
		std::cout << "Display a vector of vector of int" << std::endl;
		std::cout << c << std::endl;
	}
	std::cout << std::endl;
	
	{
		std::set<int> c;
		for (int i = 0; i < 9; ++i) { c.insert(i); }
		std::cout << "Display a set of int" << std::endl;
		std::cout << c << std::endl;
	}
	std::cout << std::endl;
	
	{
		std::map<std::string, int> c;
		c["A"] = 1;
		c["B"] = 2;
		c["C"] = 5;
		c["D"] = 3;
		std::cout << "Display a map" << std::endl;
		std::cout << c << std::endl;
	}
	std::cout << std::endl;
	
	{
		std::cout << "Display a tuple" << std::endl;
		std::cout << std::make_tuple("One tuple of", 3, "elements") << std::endl;
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp::to_string: " + hopp::to_string(nb_test) + " test fail!\n");
	
	return 0;
}
