// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <hopp/test.hpp>
#include <hopp/color.hpp>


int main()
{
	std::cout << "Test #include <hopp/color.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	++nb_test;
	{
		hopp::color c;
		std::cout << c << std::endl;
		std::cout << c.html() << std::endl;
		nb_test -= hopp::test(c.r == 0u && c.g == 0u && c.b == 0u && c.a == 255u, "hopp::color fails!\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		hopp::color c(255u, 255u, 255u);
		std::cout << c << std::endl;
		std::cout << c.html() << std::endl;
		nb_test -= hopp::test(c.r == 255u && c.g == 255u && c.b == 255u && c.a == 255u, "hopp::color fails!\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		hopp::color c(101u, 102u, 103u, 104u);
		std::cout << c << std::endl;
		std::cout << c.html() << std::endl;
		nb_test -= hopp::test(c.r == 101u && c.g == 102u && c.b == 103u && c.a == 104u, "hopp::color fails!\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		hopp::color c{105u, 106u, 107u};
		std::cout << c << std::endl;
		std::cout << c.html() << std::endl;
		nb_test -= hopp::test(c.r == 105u && c.g == 106u && c.b == 107u && c.a == 255u, "hopp::color fails!\n");
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp::color: " + hopp::to_string(nb_test) + " test fail!\n");
	
	return 0;
}
