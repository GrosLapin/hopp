// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <hopp/time/sleep.hpp>


int main()
{
	std::cout << "Sleep for 500'000'000 nanoseconds" << std::endl;
	hopp::sleep::nanoseconds(500000000);
	
	std::cout << "Sleep for 500'000 microseconds" << std::endl;
	hopp::sleep::microseconds(500000);
	
	std::cout << "Sleep for 500 milliseconds" << std::endl;
	hopp::sleep::milliseconds(500);
	
	std::cout << "Sleep for 1 seconds" << std::endl;
	hopp::sleep::seconds(1);
	
	//std::cout << "Sleep for 1 minutes" << std::endl;
	//hopp::sleep::minutes(1);
	
	//std::cout << "Sleep for 1 hours" << std::endl;
	//hopp::sleep::hours(1);
	
	return 0;
}
