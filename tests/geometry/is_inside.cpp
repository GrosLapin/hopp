// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <hopp/geometry/is_inside.hpp>
#include <hopp/test.hpp>


int main()
{
	std::cout << "Test #include <hopp/geometry/is_inside.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	// Rectangle
	
	hopp::rectangle<int> r(5, 7, 10, 3);
	std::cout << "r = " << r << std::endl;
	std::cout << std::endl;
	
	nb_test += 4;
	nb_test -= hopp::test(hopp::geometry::is_inside(5, 7, r), "Test 1 fails\n");
	nb_test -= hopp::test(hopp::geometry::is_inside(5 + 10, 7, r), "Test 2 fails\n");
	nb_test -= hopp::test(hopp::geometry::is_inside(5, 7 + 3, r), "Test 3 fails\n");
	nb_test -= hopp::test(hopp::geometry::is_inside(5 + 10, 7 + 3, r), "Test 4 fails\n");
	
	nb_test += 2;
	nb_test -= hopp::test(hopp::geometry::is_inside(6, 8, r), "Test 5 fails\n");
	nb_test -= hopp::test(hopp::geometry::is_inside(5, 7, r), "Test 6 fails\n");
	
	nb_test += 4;
	nb_test -= hopp::test(hopp::geometry::is_inside(5 - 1, 7, r) == false, "Test 7 fails\n");
	nb_test -= hopp::test(hopp::geometry::is_inside(5 + 10 + 1, 7, r) == false, "Test 8 fails\n");
	nb_test -= hopp::test(hopp::geometry::is_inside(5 - 1, 7 + 3, r) == false, "Test 9 fails\n");
	nb_test -= hopp::test(hopp::geometry::is_inside(5 + 10 + 1, 7 + 3, r) == false, "Test 9 fails\n");
	
	nb_test += 4;
	nb_test -= hopp::test(hopp::geometry::is_inside(5, 7 - 1, r) == false, "Test 10 fails\n");
	nb_test -= hopp::test(hopp::geometry::is_inside(5 + 10, 7 - 1, r) == false, "Test 11 fails\n");
	nb_test -= hopp::test(hopp::geometry::is_inside(5, 7 + 3 + 1, r) == false, "Test 12 fails\n");
	nb_test -= hopp::test(hopp::geometry::is_inside(5 + 10, 7 + 3 + 1, r) == false, "Test 13 fails\n");
	
	nb_test += 2;
	nb_test -= hopp::test(hopp::geometry::is_inside({ 5, 7 }, r), "Test 14 fails\n");
	nb_test -= hopp::test(hopp::geometry::is_inside({ 5, 7 - 1 }, r) == false, "Test 15 fails\n");
	
	return nb_test;
}
