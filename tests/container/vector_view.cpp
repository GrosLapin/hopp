// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>
#include <vector>

#include <hopp/container/vector_view.hpp>


int main()
{
	std::cout << "Test #include <hopp/container/vector_view.hpp>" << std::endl;
	
	{
		std::vector<int> const vector({ 1, 2, 3, 4, 5, 6, 7, 8, 9 });
		auto const vector_view = hopp::make_vector_view(vector, 0, vector.size());
		std::cout << vector_view << std::endl; // { 1, 2, 3, 4, 5, 6, 7, 8, 9 }
	}
	std::cout << std::endl;
	
	{
		std::vector<int> const vector({ 1, 2, 3, 4, 5, 6, 7, 8, 9 });
		auto const vector_view = hopp::make_vector_view(vector, 3, 7);
		std::cout << vector_view << std::endl; // { 4, 5, 6, 7 }
	}
	std::cout << std::endl;
	
	return 0;
}
