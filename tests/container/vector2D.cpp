// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <hopp/container/vector2D.hpp>


int main()
{
	std::cout << "Test #include <hopp/container/vector2D.hpp>" << std::endl;
	
	{
		hopp::vector2D<int> v2D;
		std::cout << v2D << std::endl;
	}
	std::cout << std::endl;
	
	{
		hopp::vector2D<int> v2D({ { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } });
		std::cout << v2D << std::endl;
	}
	std::cout << std::endl;
	
	{
		hopp::vector2D<char> const v2D({ { '0', '1', '2', '3', '4' }, { '5', '6', '7', '8', '9' } });
		for (auto const & row : v2D)
		{
			for (auto const & e : row)
			{
				std::cout << e << " ";
			}
			std::cout << std::endl;
		}
	}
	std::cout << std::endl;
	
	return 0;
}
