// Copyright © 2015 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <hopp/test.hpp>
#include <hopp/math/integer_factorization.hpp>
#include <hopp/conversion/to_string.hpp>


int main()
{
	std::cout << "Test #include <hopp/math/integer_factorization.hpp>" << std::endl;
	
	int nb_test = 0;
	
	for (long int const n : { 0l, 1l, 2l, 3l, 4l, 5l, 6l, 7l, 8l, 9l, 10l, 11l, 12l, 13l, 14l, 15l, 30l, 100l, 789l, 848994l, -468984l })
	{
		auto factors = hopp::math::integer_factorization(n);
		
		std::cout << "Factors of " << n << " =";
		for (long int const factor : factors) { std::cout << " " << factor; }
		std::cout << std::endl;
		
		++nb_test;
		{
			long int tmp = 1;
			for (long int const factor : factors) { tmp *= factor; }
			
			nb_test -= hopp::test(tmp == n, "hopp::math::integer_factorization fails");
		}
		std::cout << std::endl;
	}
	
	hopp::test(nb_test == 0, "hopp/math/integer_factorization.hpp: " + hopp::to_string(nb_test) + " test fail!\n");
	
	return 0;
}
