// Copyright © 2015 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <hopp/test.hpp>
#include <hopp/math/primes.hpp>
#include <hopp/conversion/to_string.hpp>


int main()
{
	std::cout << "Test #include <hopp/math/primes.hpp>" << std::endl;
	
	int nb_test = 0;
	
	for (int const n : { 1, 2, 3, 5, 30, 100 })
	{
		auto primes = hopp::math::primes(n);
		
		std::cout << "Primes up to " << n << " =";
		for (int const prime : primes) { std::cout << " " << prime; }
		std::cout << std::endl;
		std::cout << std::endl;
	}
	
	hopp::test(nb_test == 0, "hopp/math/primes.hpp: " + hopp::to_string(nb_test) + " test fail!\n");
	
	return 0;
}
